$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});


//   Kolkata Clock


var clockID;
var yourTimeZoneFrom = +5.50; //time zone value where you are at
var d = new Date(); //get the timezone offset

var tzDifference = yourTimeZoneFrom * 60 + d.getTimezoneOffset(); //convert the offset to milliseconds,

var offset = tzDifference * 60 * 1000;


function UpdateClock() {
    var tDate = new Date(new Date().getTime() + offset);
    var in_hours = tDate.getHours();
    var in_minutes = tDate.getMinutes();
    var in_seconds = tDate.getSeconds();
    if (in_minutes < 10) in_minutes = '0' + in_minutes;
    if (in_seconds < 10) in_seconds = '0' + in_seconds;
    if (in_hours < 10) in_hours = '0' +
        in_hours;
    document.getElementById('kolkata-time').innerHTML = "" + in_hours + ":" + in_minutes + ":" + in_seconds;
    document.getElementById('kolkata-time-2').innerHTML = "" + in_hours + ":" + in_minutes + ":" + in_seconds;
    document.getElementById('kolkata-time-3').innerHTML = "" + in_hours + ":" + in_minutes + ":" + in_seconds;
}

function StartClock() {
    clockID = setInterval(UpdateClock, 500);
}

function KillClock() {
    clearTimeout(clockID);
}
window.onload = function () {
    StartClock();
}
// end clock



$(document).ready(function () {
    $(".agent-disqualify-menu").hide("fast");
    $("#agent-disqualify-option").click(function () {
        $(".agent-disqualify-menu").toggle("slow");
    });
});
// Show Details of account withdraw-request.html
$(document).ready(function () {
    $(".details-pending-document").hide("fast");
    $("#pending-document-details-button").click(function () {
        $(".details-pending-document").toggle("slow");
    });
});
// Show Details of account document.html

$(document).ready(function () {
    $(".withdraw-request-2").hide("fast");
    $("#withdraw-request-2-button").click(function () {
        $(".withdraw-request-2").toggle("slow");
    });
});
// Show Details of account deposit.html

$(document).ready(function () {
    $(".details-deposits").hide("fast");
    $("#deposit-details-button").click(function () {
        $(".details-deposits").toggle("slow");
    });
});

$(document).ready(function () {
    $(".uploadLeads").hide("fast");
    $("#upload-leads-button").click(function () {
        $(".uploadLeads").toggle("slow");
    });
});



// PAGINATION DATA-TABLE
$(document).ready(function () {
    $('#table-scheduled-calls').DataTable({
        "paging": true,
        "ordering": false,
        "info": true,
        "searching": true,
        "pageLength": 4,
        // "dom": '<"top"i>rt<"bottom"flp><"clear">'
    });
});
$(document).ready(function () {
    $('#table-agent-lead-pool').DataTable({
        "paging": true,
        "ordering": false,
        "info": true,
        "pageLength": 4,

    });
});
$(document).ready(function () {
    $('#table-lead-history').DataTable({
        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });

});
$(document).ready(function () {
    $('#table-all-leads').DataTable({

        "paging": false,
        "ordering": false,
        "info": true,
    });

});
$(document).ready(function () {
    $('#table-registration').DataTable({

        "paging": false,
        "ordering": false,
        "info": true,
    });

});
$(document).ready(function () {
    $('#table-vicidial-users').DataTable({

        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });

});
$(document).ready(function () {
    $('#table-all-users').DataTable({

        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });


});
$(document).ready(function () {
    $('#table-withdraw-money-request').DataTable({

        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });
});

$(document).ready(function () {
    $('#table-withdraw-money-request-details').DataTable({

        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });
});
$(document).ready(function () {
    $('#pending-document-verification-list').DataTable({

        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });
});
$(document).ready(function () {
    $('#customer-deposits-table').DataTable({

        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });
});
$(document).ready(function () {
    $('#table-deposits-details').DataTable({

        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });
});
$(document).ready(function () {
    $('#table-accounts').DataTable({

        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });
});
$(document).ready(function () {
    $('#table-online').DataTable({

        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });
});
$(document).ready(function () {
    $('#table-open-position').DataTable({

        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });
});
$(document).ready(function () {
    $('#table-top-profit-position').DataTable({

        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });
});

$(document).ready(function () {
    $('#table-top-loss-position').DataTable({

        "pageLength": 5,
        "paging": true,
        "ordering": false,
        "info": true,
        "lengthChange": true
    });
});




// Search Bar
$(document).ready(function () {
    var $rows = $('#table-lead-history tr');
    $('#table-lead-history-search').keyup(function () {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
});
$(document).ready(function () {
    var $rows = $('#table-agent-lead-pool tr');
    $('#table-agent-lead-pool-search').keyup(function () {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
});

$(document).ready(function () {
    var $rows = $('#table-scheduled-calls tr');
    $('#table-scheduled-calls-search').keyup(function () {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
});
$(document).ready(function () {
    var $rows = $('#table-recently-online tr');
    $('#table-recently-online-search').keyup(function () {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
});
$(document).ready(function () {
    var $rows = $('#table-all-leads tr');
    $('#table-all-leads-search').keyup(function () {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
});

$(document).ready(function () {
    var $rows = $('#table-registration tr');
    $('#table-registration-search').keyup(function () {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
});

$(document).ready(function () {
    var $rows = $('#table-accounts tr');
    $('#table-accounts-search').keyup(function () {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
});
$(document).ready(function () {
    var $rows = $('#table-online tr');
    $('#table-online-search').keyup(function () {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
});
$(document).ready(function () {
    var $rows = $('#table-open-position tr');
    $('#table-open-position-search').keyup(function () {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
});



// Calender On Agent
$(document).ready(function () {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();


    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultView: 'agendaDay',
        height: 1055,
        aspectRatio: 1.8,
        themeSystem: 'bootstrap4',

        selectable: true,
        selectHelper: true,
        select: function (start, end, allDay) {
            var title = $("#add-event-modal").modal();
            if (title) {
                calendar.fullCalendar('renderEvent', {
                        title: title,
                        start: start,
                        end: end,
                        allDay: allDay
                    },
                    true // make the event "stick"
                );
            }
            calendar.fullCalendar('unselect');
        },
        editable: true,
        events: [{
                title: 'All Day Event',
                start: new Date(y, m, 5)
            },


        ]
    });

});

// CALL BUTTON REPLACE.
$(document).ready(function () {


    $("#call-up").click(function () {
        // $("#call-up").css("display", "none");
        $("#call-up").hide("slow");
        // $("#call-up").css("visibility", "hidden");

        // $("#hang-up").css("display", "block");

        $("#hang-up").show("slow");
        // $("#hang-up").css("visibility", "visible");

        // $("#agent-hangup-reason-list").css("display", "block");
        $("#agent-hangup-reason-list").show("slow");
        $("#text-area-hang-up-call").show("slow");
        $(".call-status").css('padding-bottom', '2%')
        $("#hang-up-submit").css('opacity', '0.2');
        $("#hang-up-submit").show("slow");




    });
});
$(document).ready(function () {


    $("#hang-up").click(function () {
        // $("#call-up").css("display", "none");
        $("#hang-up").hide("slow");
        // $("#call-up").css("visibility", "hidden");

        // $("#hang-up").css("display", "block");

        $("#redial").show("slow");
        // $("#hang-up-submit").show("slow");
        $("#hang-up-submit").show('slow').css('opacity', '1');
        $("#hang-up-submit").removeAttr("disabled");



        // $("#hang-up").css("visibility", "visible");

        // $("#agent-hangup-reason-list").css("display", "block");



    });
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
})


// Dropdown

$('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
    if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
    }
    var $subMenu = $(this).next(".dropdown-menu");
    $subMenu.toggleClass('show');


    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
        $('.dropdown-submenu .show').removeClass("show");
    });


    return false;
});

